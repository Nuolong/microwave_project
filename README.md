# Microwave

A website that tells you how long to microwave your food.

## Server API

| Request Type | Resource Endpoint | Body | Response | Description |
| --- | --- | --- | --- | --- |
| GET | /microwave/:food/:start_temp/:wattage | N/A | \{"result": "success", "time": x\} | Get time for specific food |
| GET | /microwave/reviews | N/A | \{"result": success, "reviews": \[\{"pasta": \{"good": x, "bad": y\}\}, ….\]\} | Get list of reviews |
| GET | /microwave/reviews/:food | N/A | \{"result": "success", "good": x, "bad": y\} | Get reviews for specific food |
| POST | /microwave/reviews | \{"food": "pasta", "review": "good"\} | \{"result": "success"} | Post a review |
## Testing

##### API

Test the server and API by running `python3 src/test_api.py`  
This will run unit tests for each of the different API calls

## UI  
*Index.html*
![image](https://user-images.githubusercontent.com/10440773/116830183-58d86780-ab76-11eb-9b65-a42af8e62e6d.png)  

*Result.html*
![image](https://user-images.githubusercontent.com/10440773/116830187-70175500-ab76-11eb-9d91-ca1c05f296a3.png)

## Complexity
Our project includes two HTML, two respective CSS, and two respective Javascript files. One HTML file represents the homepage which features an input and two buttons: a submit button and an expandable/collapsable settings button which further includes two settings (wattage and initial temperature) for the users to choose from. The Javascript files link the HTML to functions which make appropriate calls to our server which handles the REST API we have defined above. The Javascript functions also handle the percentage of user recommendations according to what is stored in our data and the formatting of our minutes and seconds, which our REST API initially responds with seconds.

The REST API and server, with our handler and library functions, were all written in Python. These keep track of our food data and ratios so that we can provide the accurate time the user should microwave their food for. The `reviews.py` module is our library which keeps track of every food's ratings through a JSON.

Finally, our project is designed to make it easy for expansion. We would be able to add functions like:
* More specific temperature settings
* Integrate ratings with automated calculation adjustments
* Add a food suggestion field
Given reasonable time and provided accurate data.

We overall believe our project, as a whole, is relatively complex due to its scale. We were able to take simple functionalities and transform them to a practical application through several files in our repository. We were also able to take simple functionalities and make them slightly more complex by introducing mathematical formulas.

